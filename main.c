#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "timex.h"
#include "shell.h"
#include "shell_commands.h"
#include "fmt.h"

#include "rn2xx3.h"
#include "rn2xx3_params.h"
#include "rn2xx3_internal.h"

static rn2xx3_t rn2xx3_dev;
static uint8_t payload[RN2XX3_MAX_BUF];


int main(void)
{
    puts("RN2XX3 device driver test");

    uint8_t appeui[8] = {0x70, 0xB3, 0xD5, 0x7E, 0xD0, 0x01, 0xA1, 0x0A};
    uint8_t nwkskey[16] = {0x78, 0xBE, 0x95, 0x80, 0x29, 0x11, 0x11, 0x23, 0x59, 0xBC, 0x84, 0xB1, 0xF1, 0x5E, 0x85, 0x21};
    uint8_t appskey[16] = {0xB8, 0xE4, 0x02, 0xFD, 0x07, 0xCB, 0x49, 0xE8, 0xEC, 0x08, 0xC9, 0xA5, 0x68, 0x71, 0xD0, 0x4D};
    //uint8_t nwkskey[16] = {0x21, 0x85, 0x5E, 0xF1, 0xB1, 0x84, 0xBC, 0x59, 0x23, 0x11, 0x11, 0x29, 0x80, 0x95, 0xBE, 0x78};
    //uint8_t appskey[16] = {0x4D, 0xD0, 0x71, 0x68, 0xA5, 0xC9, 0x08, 0xEC, 0xE8, 0x49, 0xCB, 0x07, 0xFD, 0x02, 0xE4, 0xB8};
    uint8_t devaddr[4] = {0x26, 0x01, 0x16, 0xA7};

    uint8_t pld[2] = {0xBB,0xBB};

    /* Built-in init */
    rn2xx3_setup(&rn2xx3_dev, &rn2xx3_params[0]);
    if (rn2xx3_init(&rn2xx3_dev) != RN2XX3_OK) {
        puts("RN2XX3 initialization failed");
        return -1;
    }

    /* Local settings (no UART involved) */
    rn2xx3_mac_set_tx_mode(&rn2xx3_dev, LORAMAC_TX_UNCNF);
    rn2xx3_mac_set_tx_port(&rn2xx3_dev, 1);

    /* TTN-specific settings */
    rn2xx3_mac_set_appeui (&rn2xx3_dev, appeui);
    xtimer_usleep(1000000);
    rn2xx3_mac_set_nwkskey (&rn2xx3_dev, nwkskey);
    xtimer_usleep(1000000);
    rn2xx3_mac_set_appskey (&rn2xx3_dev, appskey);
    xtimer_usleep(1000000);
    rn2xx3_mac_set_devaddr (&rn2xx3_dev, devaddr);
    xtimer_usleep(1000000);

    printf("Save %d\n", rn2xx3_mac_save (&rn2xx3_dev));
    xtimer_usleep(1000000);

    /* Join ABP not good, because 'frame counter' won't match with TTN's 'FC' */
    printf("Join %d\n", rn2xx3_mac_join_network	(&rn2xx3_dev, LORAMAC_JOIN_ABP));
    xtimer_usleep(1000000);

    /* Set duty cycle for first three (only active?) channels to avoid "BUSY" */
    size_t p = snprintf(
        rn2xx3_dev.cmd_buf, sizeof(rn2xx3_dev.cmd_buf) - 1, "mac set ch dcycle 0 9");
    rn2xx3_dev.cmd_buf[p] = 0;
    rn2xx3_write_cmd(&rn2xx3_dev);      // Writes from 'cmd_buf'
    xtimer_usleep(1000000);

    p = snprintf(
        rn2xx3_dev.cmd_buf, sizeof(rn2xx3_dev.cmd_buf) - 1, "mac set ch dcycle 1 9");
    rn2xx3_dev.cmd_buf[p] = 0;
    rn2xx3_write_cmd(&rn2xx3_dev);
    xtimer_usleep(1000000);

    p = snprintf(
        rn2xx3_dev.cmd_buf, sizeof(rn2xx3_dev.cmd_buf) - 1, "mac set ch dcycle 2 9");
    rn2xx3_dev.cmd_buf[p] = 0;
    rn2xx3_write_cmd(&rn2xx3_dev);
    xtimer_usleep(1000000);


    while (1) {
        /* Without resetting each time, RN2483 module will return "BUSY", till
            duty cycle allows it to re-transmit.
           With reset, it will send the packet each time, but the internal frame
            counter won't match with TTN's counter and the packet won't get
            logged for security reasosns (if chechking enabled) */
        printf("*mac_tx: %d\n", rn2xx3_mac_tx (&rn2xx3_dev, pld, 2));
        xtimer_usleep(5000000);
        //printf("*sys_reset: %d\n",rn2xx3_sys_reset (&rn2xx3_dev));
        xtimer_usleep(5000000);
        //printf("Join %d\n", rn2xx3_mac_join_network	(&rn2xx3_dev, LORAMAC_JOIN_ABP));
        xtimer_usleep(5000000);
    }

    return 0;
}
